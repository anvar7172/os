/**
 * Created by Kolibri on 26.05.2018.
 */
public class ArgsException extends java.lang.Exception {

    public ArgsException(String s) {
        super(s);
        System.out.println(s);
    }

    public ArgsException() {

    }
}
