import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by Kolibri on 18.05.2018.
 */
public class Main {
    public static void main(String[] args) throws ArgsException {
        /*
        Писатель считывает строку со станд устр ввода-вывода и передаёт её через буфер читателям.
        Читатели считывают строку из буфера и записывают её в файл
        Писатель заканчивает работа при получении quit. Читатели её в файл не пишут.

        if (args.length == 0) { throw new ArgsException("U`ve write no args");}
        if (args.length == 1) { throw new ArgsException("U have to write 2 args)");}
        if (args.length > 2) { throw new ArgsException("Too many args");}
        int count = Integer.parseInt(args[1]);
        if (count < 1) { throw new ArgsException("Count of readers is < 1");}
        */
        int count = 3;


        FileWriter fileWriter = null;
        try { fileWriter = new FileWriter(new File("C:/Users/User/Desktop/os/os/test.txt"));}
        catch (IOException e) { e.printStackTrace();}

        boolean[] readers = new boolean[count];
        for (int i = 0; i < readers.length; i++){
            readers[i]=false;}
        ReadWriteLock fileLock = new ReentrantReadWriteLock();
        Buffer buffer = new Buffer(readers);
        Writer writer = new Writer(buffer);
        writer.start();

        for (int i = 0; i < count; i++){
            Reader reader = new Reader(i, fileWriter, buffer, fileLock);
            reader.start();
        }
    }
}
