import java.util.Scanner;

/**
 * Created by Kolibri on 18.05.2018.
 */
public class Writer extends Thread {
    private Buffer buffer;


    public Writer(Buffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        Scanner in = new Scanner(System.in);
        boolean flag = false;
        while (!flag) {
            if (buffer.forReadersCheck()) {
                String read = in.nextLine();
                if (read.equals("quit")) {
                    System.out.println("Write finished");
                    flag = true;
                }

                buffer.add(read);
            }
        }
    }
}

