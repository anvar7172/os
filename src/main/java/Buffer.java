/**
 * Created by Kolibri on 18.05.2018.
 */

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Buffer {
    private ReadWriteLock bufLock = new ReentrantReadWriteLock();
    private ReadWriteLock arrLock = new ReentrantReadWriteLock();
    private boolean[] forReaders;
    private String string;

    public Buffer(boolean[] forReaders) {
        this.forReaders = forReaders;
    }

    public boolean forReadersCheck() {
        boolean flag = true;
        arrLock.readLock().lock();
        for (int i = 0; i < forReaders.length; i++) {
            if (!forReaders[i]) {
                flag = false;
                break;
            }
        }
        arrLock.readLock().unlock();
        return flag;
    }

    public void cleanReaders() {
        arrLock.writeLock().lock();
        for (int i = 0; i < forReaders.length; i++) {
            forReaders[i] = false;
        }
        arrLock.writeLock().unlock();
    }

    public void setRead(int num) {
        arrLock.writeLock().lock();
        this.forReaders[num] = true;
        arrLock.writeLock().unlock();
    }

    public void add(String string) {
        bufLock.writeLock().lock();
        this.string = string;
        bufLock.writeLock().unlock();
        cleanReaders();

    }


    public String poll(int num) {

        String elem = null;
        arrLock.readLock().lock();
        if (!forReaders[num]) {
            bufLock.readLock().lock();
            elem = string;
            bufLock.readLock().unlock();
            arrLock.readLock().unlock();

            setRead(num);
        } else {
            arrLock.readLock().unlock();
        }
        return elem;
    }
}