import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.locks.ReadWriteLock;

/**
 * Created by Kolibri on 18.05.2018.
 */
public class Reader extends Thread {
    private FileWriter fileWriter;
    private Buffer buffer;
    private int number;
    private ReadWriteLock fileLock;

    public Reader(int num, FileWriter fileWriter, Buffer buffer, ReadWriteLock fileLock) {
        this.number = num;
        this.fileWriter = fileWriter;
        this.buffer = buffer;
        this.fileLock = fileLock;
    }

    @Override
    public void run() {
        while (true) {
            String string = buffer.poll(number);
            if (string == null) {
                continue;
            }
            if (string.equals("quit")) {
                System.out.println("Reader " + number + " finished");
                return;
            }
            System.out.println("Reader " + number + " ready!");
            System.out.println("Reader " + number + " is reading " + string);
            try {
                fileLock.writeLock().lock();
                fileWriter.write(string);
                fileWriter.flush();
                fileLock.writeLock().unlock();
            } catch (IOException e) {
                e.printStackTrace();
                fileLock.writeLock().unlock();
            }
        }
    }
}

